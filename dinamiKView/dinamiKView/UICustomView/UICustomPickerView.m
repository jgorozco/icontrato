//
//  UICustomPickerView.m
//  dinamiKView
//
//  Created by pcoellov on 8/14/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "UICustomPickerView.h"

@implementation UICustomPickerView

@synthesize currentValuePicker,pckrvw;


- (id)initFromDict:(NSDictionary *)dc
{
    self = [super initFromDict:dc];
    valores = [dc objectForKey:DATE_LIST];
    currentValuePicker=@"";
    if (self)
    {
        if (([dc objectForKey:VW_X] == nil)||([dc objectForKey:VW_Y] == nil)||([dc objectForKey:VW_W] == nil)||([dc objectForKey:VW_H] == nil))
        {
            self = nil;
        }
        else {
            pckrvw = [[UIPickerView alloc] initWithFrame:[UICustomUtils rectFromDicOriginZero:dc]];
            pckrvw.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
            [pckrvw setDataSource:self];
            [pckrvw setDelegate:self];
            [pckrvw setShowsSelectionIndicator:YES];
            currentValuePicker = [dc objectForKey:PCKR_DEF];
            [self addSubview:pckrvw];
            NSLog(@"UICustomPickerView creado.");
        }
    }
    return self;
}

-(void)loadData:(NSDictionary *)datos
{
    [super loadData:datos];
    NSArray *objects = [datos objectForKey:DATE_LIST];
    for (int i=0;i<objects.count;i++)
    {
        if ([[objects objectAtIndex:i] isEqualToString:[datos objectForKey:DATE_SELECTED]])
        {
            [pckrvw selectRow:i inComponent:nil animated:YES];
        }
            
    }
}

-(NSDictionary *)getData{
    NSLog(@"dicData::: %@",dicData);
    NSLog(@"CurrentValue::: %d",posicion);
    NSDictionary *interData = [[NSDictionary alloc]initWithObjectsAndKeys:TYPE_PCKRVW ,VW_TYPE,@"Dato cargado",DATE_TEXT, nil];
    NSLog(@"interData::: %@",interData);
    //NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:key, VW_NAME, interData,DATE_DATE, nil];
    return dicData;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (valores!=nil) {
        return [valores count];
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component 
{
    if (valores!=nil) {
        return [valores objectAtIndex:row];
    }
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // TODO hacer que mande el dato para cargarlo en el getData
    posicion=row;
    //cambiamos el valor de selected de dict data
    self.currentValuePicker =[[NSString alloc]initWithString:[valores objectAtIndex:row] ];
    NSLog(@"Nombre fila::: %@",self.currentValuePicker);
}



@end
