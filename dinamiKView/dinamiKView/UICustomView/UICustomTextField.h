//
//  UICustomTextField.h
//  dinamiKView
//
//  Created by pcoellov on 8/14/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "UICustomView.h"
#import "UICustomUtils.h"

@interface UICustomTextField : UICustomView
{
    NSString *currentText;
}

@property (strong, nonatomic) UITextField *txtfld;
@property (strong, nonatomic) NSMutableArray *mutableArray;

- (id)initFromDict:(NSDictionary *)dc;
- (void)loadData:(NSDictionary *)datos;
- (NSDictionary *)getData;
-(void)muestraTextField;

@end
