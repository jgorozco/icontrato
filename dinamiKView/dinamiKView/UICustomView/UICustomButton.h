//
//  UICustomButton.h
//  dinamiKView
//
//  Created by pcoellov on 8/29/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomView.h"
#import "UICustomButtonDelegate.h"
#import "UICustomUtils.h"

@interface UICustomButton : UICustomView 
{
    NSObject<UICustomButtonDelegate> *parentHandler;
    NSDictionary *currentDictionary;
}

-(void)setParentHandler:(NSObject<UICustomButtonDelegate> *)handler;
-(void)callProtocol;

@end
