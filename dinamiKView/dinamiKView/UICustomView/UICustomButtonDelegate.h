//
//  UICustomButtonDelegate.h
//  dinamiKView
//
//  Created by pcoellov on 8/29/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UICustomButtonDelegate <NSObject>

-(void)actionSelected:(NSString *)action;

@end
