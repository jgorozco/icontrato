//
//  UICustomUtils.m
//  dinamiKView
//
//  Created by pcoellov on 8/14/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "UICustomUtils.h"

@implementation UICustomUtils

+(CGRect )rectFromDic:(NSDictionary *)dic
{
    return CGRectMake([[dic objectForKey:@"x"]intValue],
                      [[dic objectForKey:@"y"]intValue],
                      [[dic objectForKey:@"w"]intValue],
                      [[dic objectForKey:@"h"]intValue]
                      );
    
}

+(CGRect)rectFromDicOriginZero:(NSDictionary *)dic
{
    return CGRectMake(0,0,[[dic objectForKey:@"w"]intValue],
                          [[dic objectForKey:@"h"]intValue]
                          );
}

+(CGRect)rectFromFrameOriginZero:(CGRect)rect
{
    return CGRectMake(0,0, rect.size.width, rect.size.height);
}

+(NSDictionary *)JSONtoDict:(NSDictionary *)metaElements
{
    NSMutableDictionary *back = [[NSMutableDictionary alloc]init];
    for (NSDictionary *d in [metaElements objectForKey:DATE_NAME]) 
    {
        [back setObject:[d objectForKey:DATE_DATE] forKey:[d objectForKey:VW_NAME]];
    }
    return  back;
}

+(UIColor *) colorFromHexString:(NSString *)hexString
{
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+(BOOL)validateString:(NSString *)data type:(NSString *)type
{
    
    if (!data)
        return NO;
    if ([type isEqualToString:TF_VALIDATION_MAIL])
    {
        BOOL stricterFilter = YES;
        NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
        NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        return [emailTest evaluateWithObject:data];
    }
    
    if ([type isEqualToString:TF_VALIDATION_NUM])
    {
        bool result = false;
        NSScanner *scan = [NSScanner scannerWithString:data];
        if (![scan scanFloat:NULL] || ![scan isAtEnd])
        {
            result=NO;
        }
        else
        {
            result=YES;
        }
        return result;
    }
    
    return YES;
    
}

- (UIImage *)imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

+ (BOOL)validateSign:(PaintView *)firma
{
    return [firma isSigned];
}

+(BOOL)validateMinimum:(int)valor :(int)minimo
{
    BOOL mayor = FALSE;
    if (valor >= minimo)
    {
        mayor = TRUE;
    }
    return mayor;
}

+(NSArray *)getNameColumns:(UIView *)vista
{
    NSMutableArray *nombres = [[NSMutableArray alloc] init];
    NSArray *botones = [vista subviews];
    for (int i=0;i<botones.count;i++)
    {
        UIButton *btn = [botones objectAtIndex:i];
        NSString *txt = btn.titleLabel.text;
        [nombres addObject:txt];
    }
    return nombres;
}

+(void)sort:(NSString *)tabla
{
    
}

@end
