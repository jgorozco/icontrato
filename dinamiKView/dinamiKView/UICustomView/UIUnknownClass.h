//
//  UIUnknownClass.h
//  UIUnknownClass
//
//  Created by pcoellov on 8/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UICustomView.h"
#import "Constants.h"

@interface UIUnknownClass : UICustomView

@property (strong, nonatomic) UIImageView *vistaImagen;

@end
