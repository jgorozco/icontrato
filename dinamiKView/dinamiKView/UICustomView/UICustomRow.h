//
//  UICustomRow.h
//  dinamiKView
//
//  Created by pcoellov on 9/6/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomView.h"
#import "UICustomUtils.h"
#import "Constants.h"

@interface UICustomRow : UICustomView
{
    NSMutableDictionary *vistas;

}
@property (strong, nonatomic) UIView *view;

- (id)initFromDict:(NSDictionary *)dc;
-(void)setMyDelegate:(id)delegado;
@end
