//
//  UICustomDatePicker.h
//  dinamiKView
//
//  Created by pcoellov on 8/14/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "UICustomView.h"
#import "UICustomUtils.h"

@interface UICustomDatePicker : UICustomView

@property (strong, nonatomic) UIDatePicker *dtpckr;

- (id)initFromDict:(NSDictionary *)dc;
- (void)loadData:(NSDictionary *)datos;
- (NSDictionary *)getData;

@end
