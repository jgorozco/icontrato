//
//  PaintView.h
//  PaintApp
//
//  Created by Carlos Balduz Bernal on 03/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomView.h"
#import "UICustomUtils.h"

@interface PaintView : UICustomView {
    CGPoint previousPoint;
    CGPoint lastPoint;
    CGMutablePathRef path;
    BOOL wasSigned;
}
-(void)beginDraw:(CGPoint)point;
-(void)moveDraw:(CGPoint)point;
-(void)clearDraw:(id)sender;
-(BOOL)isSigned;
-(UIButton *)botonClear: (CGRect)frame;
//-(void)setImage:(UIImage *)imagen;
-(void) setView: (UIImage *) imagen;
@end
