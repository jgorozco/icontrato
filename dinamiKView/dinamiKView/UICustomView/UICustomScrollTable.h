//
//  UICustomScrollTable.h
//  customTable
//
//  Created by pcoellov on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomView.h"
#import "Constants.h"
#import "UICustomUtils.h"
#import "UICustomRow.h"

@interface UICustomScrollTable : UICustomView  <UITextFieldDelegate>
{
    NSString *currentTableName;
    NSMutableDictionary *dicDatosTabla;
    NSMutableDictionary *dicDatosVisibles;
    NSMutableArray *celdas;
    float heightRow;
}

@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIView *header;
@property (strong, nonatomic) IBOutlet UIScrollView *filas;
@property (nonatomic) BOOL *ordenado;
@property (strong ,nonatomic)UIButton *mas;



-(id)initFromDict:(NSDictionary *)dc;
-(void)loadData:(NSDictionary *)datos;
-(NSDictionary *)setDatosTabla:(NSDictionary *)datos;

@end
