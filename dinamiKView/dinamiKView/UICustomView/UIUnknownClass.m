//
//  UIUnknownClass.m
//  UIUnknownClass
//
//  Created by pcoellov on 8/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIUnknownClass.h"

@interface UIUnknownClass ()

@end

@implementation UIUnknownClass

@synthesize vistaImagen;

- (id)initFromDict:(NSDictionary *)dc
{
    self = [super initFromDict:dc];
    if (self) {
        int w = [[dc objectForKey:VW_W]intValue];
        int h = [[dc objectForKey:VW_H]intValue];
        if (([dc objectForKey:VW_W] == nil) || (([dc objectForKey:VW_H] == nil)))
        {   
            w = 140;
            h = 65;
        }
        CGRect marco = CGRectMake([[dc objectForKey:VW_X]intValue],[[dc objectForKey:VW_Y]intValue],w,h);
        [self setFrame:marco];
    }
    NSLog(@"UIUnknownClass creado.");
    return self;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end
