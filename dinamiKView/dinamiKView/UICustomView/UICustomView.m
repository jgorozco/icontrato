//
//  UICustomView.m
//  dinamiKView
//
//  Created by pcoellov on 8/10/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "UICustomView.h"
#import "UICustomUtils.h"

@implementation UICustomView

-(id)initFromDict:(NSDictionary *)dc
{   
    dicConfig = dc;
    self = [super initWithFrame:[UICustomUtils rectFromDic:dc]];
     if (self)
     { 
         //self.backgroundColor = [UIColor greenColor];
     }
    return self;
}
-(NSString *)getName
{

    return [dicConfig objectForKey:VW_NAME];
}

-(void)loadData:(NSDictionary *)datos
{
    dicData = [NSMutableDictionary dictionaryWithDictionary:datos];
}

-(NSDictionary *)getData
{
    return dicData;
}

-(BOOL)validate
{
    return true;
}

-(void)setReciver:(NSObject *)recibidor
{
    mirecibidor=recibidor;
    
}


@end
