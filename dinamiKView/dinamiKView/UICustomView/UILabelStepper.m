//
//  UILabelStepper.m
//  dinamiKView
//
//  Created by jose garcia on 12/06/12.
//  Copyright (c) 2012 freelance. All rights reserved.
//

#import "UILabelStepper.h"

@implementation UILabelStepper
@synthesize steeperMax;
@synthesize view;
@synthesize btnMenos;
@synthesize btnMas;
@synthesize labelContenido;
@synthesize labelTitulo;

- (id)initFromDict:(NSDictionary *)dc
{
    self = [super initFromDict:dc];
    if (self) 
    {
        if (([dc objectForKey:LS_TITLE] == nil)||([dc objectForKey:LS_SUBTITLE] == nil)||([dc objectForKey:VW_X] == nil)||([dc objectForKey:VW_Y] == nil)||([dc objectForKey:VW_W] == nil)||([dc objectForKey:VW_H] == nil))
        {
            self = nil;
        }
        else {
            UIView *vista = self.view;
            [[NSBundle mainBundle] loadNibNamed:@"UILabelStepper" owner:self options:nil];
            [self addSubview:self.view];
            //self.backgroundColor = [UIColor greenColor];
            [self setCurrentValue:[[dc objectForKey:DATE_NUM_INIT]intValue]];
            [self setTitle:[dc objectForKey:LS_TITLE]];
            self.labelTitulo.textColor = [UIColor darkTextColor];
            [self setSubTitle:[dc objectForKey:LS_SUBTITLE]];
            self.steeperMax.textColor = [UIColor darkTextColor];
            [vista addSubview:self];
            vista = self.view;
            vista.frame = [UICustomUtils rectFromFrameOriginZero:self.frame];
            //vista.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
            view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            NSLog(@"LabelStepper creado.");
        }
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame 
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        // Initialization code.
        [[NSBundle mainBundle] loadNibNamed:@"UILabelStepper" owner:self options:nil];
        [self addSubview:self.view];
        currentValue=0;
    }
    return self;
}

- (void)awakeFromNib
{
    [[NSBundle mainBundle] loadNibNamed:@"UILabelStepper" owner:self options:nil];
    [self addSubview:self.view];
    [labelContenido setText:[NSString stringWithFormat:@"%d",currentValue]];

}

-(void)setTitle:(NSString *)title
{
    labelTitulo.text=title;
}

-(void)setSubTitle:(NSString *)title
{
    steeperMax.text=title;
}

-(void)setCurrentValue:(int)value
{
    labelContenido.text = [NSString stringWithFormat:@"%d",value];
    currentValue = value;
}


-(void)clearValue
{
    currentValue=0;
      [labelContenido setText:[NSString stringWithFormat:@"%d",currentValue]];  
}

- (IBAction)clickMenos:(id)sender {
    if (currentValue==0)
        currentValue=0;
    else
        currentValue=currentValue-1;
    [labelContenido setText:[NSString stringWithFormat:@"%d",currentValue]];
}

- (IBAction)clickMas:(id)sender {
        currentValue=currentValue+1;
    [labelContenido setText:[NSString stringWithFormat:@"%d",currentValue]];
    
}

-(int)getValue
{
    return currentValue;
}

-(NSDictionary *)getData
{
    [super getData];
    UILabel *labelText = [[[[self subviews]objectAtIndex:0]subviews]objectAtIndex:1];
    NSString *text = labelText.text;
    NSDictionary *interData = [[NSDictionary alloc]initWithObjectsAndKeys:TYPE_LBLSTPR ,VW_TYPE,text,DATE_TEXT, nil];
    //NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:key, VW_NAME, interData,DATE_DATE, nil];
    return dicData;
}

@end
