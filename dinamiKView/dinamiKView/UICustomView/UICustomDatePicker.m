//
//  UICustomDatePicker.m
//  dinamiKView
//
//  Created by pcoellov on 8/14/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "UICustomDatePicker.h"

@implementation UICustomDatePicker

@synthesize dtpckr;

- (id)initFromDict:(NSDictionary *)dc
{
    self = [super initFromDict:dc];
    if (self)
    {
        if (([dc objectForKey:VW_X] == nil)||([dc objectForKey:VW_Y] == nil)||([dc objectForKey:VW_W] == nil)||([dc objectForKey:VW_H] == nil))
        {
            self = nil;
        }
        else {
            dtpckr = [[UIDatePicker alloc] initWithFrame:[UICustomUtils rectFromDicOriginZero:dc]];
            if ([[dc objectForKey:DP_FORMAT]isEqualToString:DP_FORMAT_DATE]) 
                dtpckr.datePickerMode = UIDatePickerModeDate;
            if ([[dc objectForKey:DP_FORMAT]isEqualToString:DP_FORMAT_DATE_TIME]) 
                dtpckr.datePickerMode = UIDatePickerModeDateAndTime;
            if ([[dc objectForKey:DP_FORMAT]isEqualToString:DP_FORMAT_TIME]) 
                dtpckr.datePickerMode = UIDatePickerModeTime;
            if ([[dc objectForKey:DP_FORMAT]isEqualToString:DP_FORMAT_TIMER]) 
                dtpckr.datePickerMode = UIDatePickerModeCountDownTimer;
            [self addSubview:dtpckr];
            NSLog(@"UICustomDatePicker creado.");
        }
    }
    return self;
}


- (void)loadData:(NSDictionary *)datos
{
    [super loadData:datos];
    NSDateFormatter *dtfrmt = [[NSDateFormatter alloc] init];
    [dtfrmt setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [[NSDate alloc]init];	
    date = [dtfrmt dateFromString:[datos objectForKey:DATE_TEXT]];
    [dtpckr setDate:date];
}

- (NSDictionary *)getData
{
    [super getData];
    NSDate *mifecha = dtpckr.date;
    NSString *stringfecha = [NSString stringWithFormat:@"%@",mifecha];
    NSString *soloFecha = [stringfecha substringToIndex:10];
    NSDictionary *interData = [[NSDictionary alloc]initWithObjectsAndKeys:TYPE_DTPCKR ,VW_TYPE,soloFecha,DATE_TEXT, nil];
    //NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:key, VW_NAME, interData,DATE_DATE, nil];
    return dicData;
}

@end
