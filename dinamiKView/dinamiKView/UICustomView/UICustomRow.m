//
//  UICustomRow.m
//  dinamiKView
//
//  Created by pcoellov on 9/6/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "UICustomRow.h"

@interface UICustomRow ()

@end

@implementation UICustomRow

@synthesize view;

- (id)initFromDict:(NSDictionary *)dc
{
    [dc setValue:[NSString stringWithFormat:@"%d",0] forKey:VW_X];
    [dc setValue:[NSString stringWithFormat:@"%d",0] forKey:VW_Y];
    [dc setValue:[dc objectForKey:TABLE_H_ROW] forKey:VW_H];
    self = [super initFromDict:dc];
    if (self) {
        vistas=[[NSMutableDictionary alloc]init ];
        int x = [[dc objectForKey:VW_X]intValue];
        int y = [[dc objectForKey:TABLE_F_ROW]intValue];
        int h = [[dc objectForKey:VW_H]intValue];
        NSLog(@"columnas:%@",[dc objectForKey:TABLE_COL_NUMBER]);
        for ( NSDictionary *dicCol in [dc objectForKey:TABLE_COLUMNS])
        {
            CGRect frametxt = CGRectMake(x, y, [[dicCol objectForKey:VW_W]floatValue], h);
            x = x + [[dicCol objectForKey:VW_W]intValue];
            UITextField *txt = [[UITextField alloc]init];
            [txt setFrame:frametxt];
            [txt setBorderStyle:UITextBorderStyleBezel];
            [txt setBackgroundColor:[UIColor whiteColor]];
            [txt setContentMode:UIViewContentModeCenter];
            [vistas setObject:txt forKey:[dicCol objectForKey:@"name"]];
            [self addSubview:txt];
    
        } 
    }
    return self;
}

-(void)loadData:(NSDictionary *)datos
{
 //   NSLog(@"DATOS:%@",datos);
    for (NSString *key in datos.allKeys) {
        UITextField *txr =[vistas objectForKey:key];
        if ((txr)&&([txr isKindOfClass:[UITextField class]]))
        {
            id dato=[datos objectForKey:key];
            if (![dato isKindOfClass:[NSString class]])
            {
                dato=[dato stringValue];
            }
            [txr setText:dato];
        }
    }


}
-(void)setMyDelegate:(id)delegado{
    for (id tf in [vistas allValues])
    {
        if ([tf isKindOfClass:[UITextField class]]){
            
            [tf setDelegate:delegado];
        }
        //
    
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end
