//
//  UICustomSlider.h
//  dinamiKView
//
//  Created by pcoellov on 8/14/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomView.h"
#import "UICustomUtils.h"
#import "Constants.h"

@interface UICustomSlider : UICustomView

- (id)initFromDict:(NSDictionary *)dc;
- (NSDictionary *)getData;

@end
