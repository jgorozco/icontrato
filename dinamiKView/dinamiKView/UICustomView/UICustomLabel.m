//
//  UICustomLabel.m
//  dinamiKView
//
//  Created by pcoellov on 8/14/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "UICustomLabel.h"

@implementation UICustomLabel

- (id)initFromDict:(NSDictionary *)dc
{
    self = [super initFromDict:dc];
    if (self)
    {
        if (([dc objectForKey:TF_TXT] == nil)||([dc objectForKey:VW_X] == nil)||([dc objectForKey:VW_Y] == nil)||([dc objectForKey:VW_W] == nil)||([dc objectForKey:VW_H] == nil))
        {
            self = nil;
        }
        else {
            UILabel *lbl = [[UILabel alloc] initWithFrame:[UICustomUtils rectFromDicOriginZero:dc]];
            NSString *value=[dc objectForKey:TF_TXT];
            if (value)
                [lbl setText:value];
            value=[dc objectForKey:VW_COLOR];
            if (value)
                [lbl setBackgroundColor:[UICustomUtils colorFromHexString:value]];
            value=[dc objectForKey:VW_TXT_COLOR];
            if (value)
                [lbl setTextColor:[UICustomUtils colorFromHexString:value]];
            value=[dc objectForKey:LB_FONTSIZE];
            if (value)
                lbl.font = [UIFont fontWithName:@"Helvetica" size:[value intValue]];
            [self addSubview:lbl];
            NSLog(@"UICustomLabel creado.");
        }
    }
    return self;
}

@end
