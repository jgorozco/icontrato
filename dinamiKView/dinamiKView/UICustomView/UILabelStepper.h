//
//  UILabelStepper.h
//  dinamiKView
//
//  Created by jose garcia on 12/06/12.
//  Copyright (c) 2012 freelance. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "UICustomView.h"
#import "UICustomUtils.h"

@interface UILabelStepper : UICustomView
{
    int currentValue;

}
@property (strong, nonatomic) IBOutlet UILabel *steeperMax;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIButton *btnMenos;
@property (strong, nonatomic) IBOutlet UIButton *btnMas;
@property (strong, nonatomic) IBOutlet UILabel *labelContenido;
@property (strong, nonatomic) IBOutlet UILabel *labelTitulo;

- (id)initFromDict:(NSDictionary *)dc;
- (void)setSubTitle:(NSString *)title;
- (void)setTitle:(NSString *)title;
- (void)setCurrentValue:(int)value;
- (IBAction)clickMenos:(id)sender;
- (IBAction)clickMas:(id)sender;
- (void)clearValue;
- (int)getValue;
- (NSDictionary *)getData;

@end
