//
//  UICustomUtils.h
//  dinamiKView
//
//  Created by pcoellov on 8/14/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/CALayer.h>
#import "ViewController.h"
#import "PaintView.h"
#import "UICustomUtils.h"
#import "UICustomTextField.h"
#import "UICustomLabel.h"
#import "PaintView.h"
#import "UILabelStepper.h"
#import "UICustomSlider.h"
#import "UICustomDatePicker.h"
#import "UILabelSlider.h"
#import "UICustomPickerView.h"
#import "UICustomScrollTable.h"

@interface UICustomUtils : NSObject

+(CGRect)rectFromDic:(NSDictionary *)dic;
+(CGRect)rectFromDicOriginZero:(NSDictionary *)dic;
+(CGRect)rectFromFrameOriginZero:(CGRect)dic;
+(NSDictionary *)JSONtoDict:(NSDictionary *)metaElements;
//+(UICustomView *)customFromDic:(NSDictionary *)dic;
+(UIColor *)colorFromHexString:(NSString *)hexString;
+(UIImage *)imageWithView:(UIView *)view;
+(NSArray *)getNameColumns:(UIView *)vista;
+(void)sort:(NSString *)tabla;

+(BOOL)validateString:(NSString *)data type:(NSString *)type;
+(BOOL)validateSign:(UICustomView *)firma;
+(BOOL)validateMinimum:(int)valor:(int)minimo;

@end
