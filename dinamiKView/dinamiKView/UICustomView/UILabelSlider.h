//
//  UILabelSlider.h
//  UILabelSlider
//
//  Created by pcoellov on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UICustomView.h"
#import "UICustomUtils.h"
#import "Constants.h"

@interface UILabelSlider : UICustomView
{
    float currentValue;
}

@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UILabel *sliderMax;
@property (strong, nonatomic) IBOutlet UILabel *sliderMin;
@property (strong, nonatomic) IBOutlet UILabel *sliderCurrent;
@property (strong, nonatomic) IBOutlet UISlider *slider;

-(id)initWithFrame:(CGRect)frame;
-(void)setMaxValue:(int)max;
-(void)setMinValue:(int)min;
-(void)setCurrentValue:(float)current;
-(float)getCurrent;

-(void)loadData:(NSDictionary *)datos;
-(NSDictionary *)getData;

@end
