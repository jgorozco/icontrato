//
//  UICustomTextField.m
//  dinamiKView
//
//  Created by pcoellov on 8/14/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "UICustomTextField.h"

@implementation UICustomTextField

@synthesize txtfld,mutableArray;

- (id)initFromDict:(NSDictionary *)dc
{
    self = [super initFromDict:dc];
    if (self)
    {
        if (([dc objectForKey:VW_X] == nil)||([dc objectForKey:VW_Y] == nil)||([dc objectForKey:VW_W] == nil)||([dc objectForKey:VW_H] == nil))
        {
            self = nil;
        }
        else {
            txtfld=[[UITextField alloc] initWithFrame:[UICustomUtils rectFromDicOriginZero:dc]];
            NSString *value=[dc objectForKey:TF_STYLE];
            if ((value)&&([value isEqualToString:TF_STYLE_ROUND_RECT]))
                [txtfld setBorderStyle:UITextBorderStyleRoundedRect];
            value=[dc objectForKey:TF_TXT];
            if (value)
                [txtfld setText:value];
            value=[dc objectForKey:TF_PHOLDER];
            if (value)
                [txtfld setPlaceholder:value];
            value=[dc objectForKey:VW_COLOR];
            if (value)
                [txtfld setBackgroundColor:[UICustomUtils colorFromHexString:value]];
            value=[dc objectForKey:VW_TXT_COLOR];
            if (value)
                [txtfld setTextColor:[UICustomUtils colorFromHexString:value]];
        
            value=[dc objectForKey:VW_VALIDATION];
            if ((value)&&([value isEqualToString:TF_VALIDATION_MAIL]))
            {
                [txtfld setKeyboardAppearance:UIKeyboardAppearanceDefault];
                [txtfld setKeyboardType:UIKeyboardTypeEmailAddress];
            }
            if ((value)&&([value isEqualToString:TF_VALIDATION_NUM]))
            {
                [txtfld setKeyboardAppearance:UIKeyboardAppearanceDefault];
                [txtfld setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
            }
            [self addSubview:txtfld];
            NSLog(@"UICustomTextField creado.");
        }
    }
    return self;
}

-(void)loadData:(NSDictionary *)datos{
    NSLog(@"Datos que entran::: %@",datos);
    [super loadData:datos];
    NSLog(@"Array::: %@",mutableArray);
    if ([[datos objectForKey:TF_TXT]isKindOfClass:[NSNumber class]])
        [self.txtfld setText:[[datos objectForKey:TF_TXT]stringValue]];
    else 
        [self.txtfld setText:[datos objectForKey:TF_TXT]];     
}

-(void)muestraTextField
{
    NSLog(@"Muestra::: %@",mutableArray);
}

-(NSDictionary *)getData
{
    [super getData];
    UITextField *txt = [self.subviews objectAtIndex:0];
    NSString *dataText = txt.text;
    NSDictionary *interData = [[NSDictionary alloc]initWithObjectsAndKeys:TYPE_TF ,VW_TYPE,dataText,DATE_TEXT, nil];
    //NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:key, VW_NAME, interData,DATE_DATE, nil];
    return dicData;
}

@end
