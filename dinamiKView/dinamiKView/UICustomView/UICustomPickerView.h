//
//  UICustomPickerView.h
//  dinamiKView
//
//  Created by pcoellov on 8/14/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomView.h"
#import "Constants.h"
#import "UICustomUtils.h"

@interface UICustomPickerView : UICustomView <UIPickerViewDelegate, UIPickerViewDataSource>
{
    NSArray *valores;
    int posicion;
}

@property (nonatomic,retain) NSString *currentValuePicker;
@property (strong, nonatomic) UIPickerView *pckrvw;

- (id)initFromDict:(NSDictionary *)dc;
- (void)loadData:(NSDictionary *)datos;
- (NSDictionary *)getData;

@end
