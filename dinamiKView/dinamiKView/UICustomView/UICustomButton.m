//
//  UICustomButton.m
//  dinamiKView
//
//  Created by pcoellov on 8/29/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "UICustomButton.h"

@interface UICustomButton ()

@end

@implementation UICustomButton

-(id)initFromDict:(NSDictionary *)dc
{
    self = [super initFromDict:dc];
    currentDictionary = dc;
    if (self) 
    {
        if (([dc objectForKey:VW_X] == nil)||([dc objectForKey:VW_Y] == nil)||([dc objectForKey:VW_W] == nil)||([dc objectForKey:VW_H] == nil))
        {
            self = nil;
        }
        else {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [btn setTitle:[dc objectForKey:BTN_TEXT] forState:UIControlStateNormal];
            [btn setTitle:[dc objectForKey:BTN_TEXT] forState:UIControlStateSelected];
            btn.frame = [UICustomUtils rectFromDicOriginZero:dc];
            [btn addTarget:self action:@selector(callProtocol) forControlEvents:UIControlEventTouchUpInside];
            self.backgroundColor = [UIColor clearColor];
            [self addSubview:btn];
            NSLog(@"UICustomButton creado.");
        }
    }
    return self;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}
         
- (void)setParentHandler:(NSObject<UICustomButtonDelegate> *)handler
{
    parentHandler=handler;
}

-(void)callProtocol
{
    [parentHandler actionSelected:[currentDictionary objectForKey:BTN_ACT]];
}

@end
