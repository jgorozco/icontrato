//
//  UICustomScrollTable.m
//  customTable
//
//  Created by pcoellov on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "UICustomScrollTable.h"

@interface UICustomScrollTable ()

@end

@implementation UICustomScrollTable

@synthesize view,header,filas,ordenado,mas;
//@synthesize dicDatosTabla,dicDatosVisibles;

-(NSMutableDictionary *)setDatosTabla:(NSMutableDictionary *)datos
{
    dicDatosTabla = datos;
    return dicDatosTabla;
}
- (id)initFromDict:(NSDictionary *)dc
{
    self = [super initFromDict:dc];
    self.backgroundColor = [UIColor clearColor];
    if (self)
    {
    [[NSBundle mainBundle] loadNibNamed:@"UICustomScrollTable" owner:self options:nil];
        heightRow=[[dc objectForKey:TABLE_H_ROW] intValue];
        [self createHeader:dc];
        [self createRows:dc];
      //  [self createRowContainer:dc];

            }
    return self;
}

-(void)createHeader:(NSDictionary *)dc
{
    CGRect headframe = CGRectMake(self.header.frame.origin.x,
                                  self.header.frame.origin.y,
                                  [[dc objectForKey:VW_W]floatValue],
                                  self.header.frame.size.height);
    [self.header setFrame:headframe];
    int x = 0;
    NSArray *columnas = [dc objectForKey:TABLE_COLUMNS];
    for (int i=0;i<[[dc objectForKey:TABLE_COL_NUMBER]intValue];i++)
    {
        NSDictionary *dicCol = [columnas objectAtIndex:i];
        CGRect boton = CGRectMake(x, 0,[[dicCol objectForKey:VW_W]intValue], 37);
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btn addTarget:self action:@selector(ordenar:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitle:[dicCol objectForKey:VW_NAME] forState:UIControlStateNormal];
        btn.frame = boton;
        x = x + [[dicCol objectForKey:VW_W]intValue];
        self.header.backgroundColor = [UIColor clearColor];
        [self.header addSubview:btn];
        [self addSubview:self.header];
    }

}

-(void)createRowContainer:(NSDictionary *)dc
{

}

-(void)createRows:(NSDictionary *)dc
{
    celdas=[[NSMutableArray alloc] init];
    CGRect headFilas = CGRectMake(self.filas.frame.origin.x,
                                  self.header.frame.size.height,
                                  [[dc objectForKey:VW_W]floatValue],
                                  [[dc objectForKey:VW_H]floatValue]);
    [self.filas setFrame:headFilas];
    mas = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [mas addTarget:self action:@selector(addRow:) forControlEvents:UIControlEventTouchUpInside];
    int x = self.view.frame.size.width-2.0-37.0*1.5;
    CGRect boton = CGRectMake(x,[celdas count]*heightRow, 37.0, 37.0);
    [mas setTitle:@"+" forState:UIControlStateNormal];
    [mas setFrame:boton];
    [mas setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [filas setBackgroundColor:[UIColor blackColor]];
    [self.filas addSubview:mas];
  //  [self.filas setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.filas];

}
-(void)repositionPlusButton
{
    int x = self.view.frame.size.width-2.0-37.0*1.5;
    CGRect boton = CGRectMake(x,[celdas count]*heightRow, 37.0, 37.0);
    [self.filas setContentSize:(CGSizeMake(self.filas.frame.size.width, boton.size.height*2+boton.origin.y))];
    NSLog(@"contentsize:%@",NSStringFromCGSize(self.filas.contentSize));
    [mas setFrame:boton];
}


-(void)loadData:(NSDictionary *)datos
{
    dicData=[[NSMutableDictionary alloc] initWithDictionary:datos];
    NSArray *elems=[dicData objectForKey:DATE_DATE];
    for (NSDictionary  *d in elems)
    {
        [self addRow:d];
    
    
    }
    
    
}
-(void)addRow:(NSDictionary *)dict
{
    UICustomRow *row = [[UICustomRow alloc]initFromDict:dicConfig];
    [row setFrame:CGRectMake(row.frame.origin.x,
                             [celdas count]*heightRow,
                             row.frame.size.width,
                             row.frame.size.height)];
    [filas addSubview:row];
    [celdas addObject:row];
   
    if ((dict)&&([dict isKindOfClass:[NSDictionary class]]))
    {
        [row loadData:dict];
    }
    [row setMyDelegate:mirecibidor];
    [self repositionPlusButton];

}



-(void)loadData2:(NSDictionary *)datos
{
    dicDatosVisibles = [[NSMutableDictionary alloc] init];
    dicDatosTabla = [[NSMutableDictionary alloc]init];
    
    NSMutableDictionary *d = [[NSMutableDictionary alloc]initWithDictionary:datos];
    [self setDatosTabla:d];
    NSArray *tuplas = [datos objectForKey:DATE_DATE];
    NSArray *nombres = [UICustomUtils getNameColumns:self.header];
    for (int i=0;i<tuplas.count;i++)
    {
        NSDictionary *datosTupla = [tuplas objectAtIndex:i];
        for (int j=0;j<nombres.count;j++)
        {
            NSString *insert = [NSString stringWithFormat:@"%@",[datosTupla objectForKey:[nombres objectAtIndex:j]]];
        //    [textfield setText:insert];
        }
    }
    NSLog(@"Datos tabla loadata::: %@",dicDatosTabla);
    //[dicDatosTabla setObject:tuplas forKey:DATE_DATE];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return NO;
}

-(void)ordenar:(id)sender
{
    // TODO conseguir que se llame al dicDatos y ordenarlo para mostrarlo
    
    /*
    NSLog(@"Dicdatos inicio::: %@",dicDatosTabla);
    UIButton *btn = (UIButton *)sender;
    NSString *columna = btn.titleLabel.text;
    if (![columna isEqualToString:currentColumn])
    {
        [self setOrdenado:FALSE];
    }
    if (ordenado)
    {
        NSLog(@"Colocar en orden inverso.");
        [self setOrdenado:FALSE];
    }
    else {
        NSLog(@"Ordenar elementos de las filas.");
        dicDatosVisibles = dicDatosTabla;
        NSMutableArray *sinOrden = [[NSMutableArray alloc] init];
        for (NSDictionary *elem in dicDatosVisibles)
        {
            [sinOrden addObject:[elem objectForKey:columna]];
        }
        [self setOrdenado:TRUE];
    }
    currentColumn = columna;
    */
}
-(int)getRows
{
    NSString *jsonPath=[[NSBundle mainBundle] pathForResource:@"dataTable" ofType:@"json"];
    NSDictionary *metaElements =[NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:jsonPath] options:NSJSONReadingMutableContainers error:nil];
    NSDictionary *poks = [metaElements objectForKey:DATE_DATE];
    return poks.count;
}

-(int)getColumns
{
    NSString *jsonPath=[[NSBundle mainBundle] pathForResource:@"formTable" ofType:@"json"];
    NSDictionary *metaElements =[NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:jsonPath] options:NSJSONReadingMutableContainers error:nil];
    NSDictionary *dic = [metaElements objectForKey:currentTableName];
    return [[dic objectForKey:TABLE_COL_NUMBER]intValue];
}





@end
