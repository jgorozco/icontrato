//
//  UILabelSlider.m
//  UILabelSlider
//
//  Created by pcoellov on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UILabelSlider.h"

@interface UILabelSlider () 

@end

@implementation UILabelSlider

@synthesize view,sliderMax,sliderMin,slider,sliderCurrent;


- (id)initFromDict:(NSDictionary *)dc 
{
    self = [super initFromDict:dc];
    if (self) {
        int fmax = [[dc objectForKey:SLDR_MAX]intValue];
        int fmin = [[dc objectForKey:SLDR_MIN]intValue];
        if (([dc objectForKey:SLDR_MAX] == nil)||([dc objectForKey:SLDR_MIN] == nil)||([dc objectForKey:VW_X] == nil)||([dc objectForKey:VW_Y] == nil)||([dc objectForKey:VW_W] == nil)||([dc objectForKey:VW_H] == nil))
        {
            self = nil;
        }
        else {
            UIView *vista = self.view;
            [[NSBundle mainBundle] loadNibNamed:@"UILabelSlider" owner:self options:nil];
            [self addSubview:self.view];
            [self setMaxValue:fmax];
            [self setMinValue:fmin];
            [vista addSubview:self];
            vista = self.view;
            //vista.backgroundColor = [UIColor greenColor];
            vista.frame = [UICustomUtils rectFromFrameOriginZero:self.frame];
            vista.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            NSLog(@"UILabelSlider creado.");
        }
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        // Initialization code.
        [[NSBundle mainBundle] loadNibNamed:@"UILabelSlider" owner:self options:nil];
        [self addSubview:self.view];
        [sliderCurrent setText:[NSString stringWithFormat:@"%d",currentValue]];
        [self setCurrentValue:currentValue];
    }
    return self;
}


-(void)setMaxValue:(int)max
{
    NSString *maximum = [NSString stringWithFormat:@"%d",max];
    self.sliderMax.text = maximum;
    [slider setMaximumValue:max];
}

-(void)setMinValue:(int)min
{
    NSString *minimum = [NSString stringWithFormat:@"%d",min];
    self.sliderMin.text = minimum;
    [slider setMinimumValue:min];
}

-(void)setCurrentValue:(float)current
{
    NSNumber *num = [NSNumber numberWithFloat:current];
    NSString *actual = [NSString stringWithFormat:@"%@",num];
    self.sliderCurrent.text = actual;
    [slider setValue:current];
}

-(float)getCurrent
{
    return currentValue;
}

-(IBAction)sliderValueChanged:(UISlider *)sender
{
    float valor = [sender value];
    int numero = (int)valor;
    NSString *value = [NSString stringWithFormat:@"%d",numero];
    self.sliderCurrent.text = value;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void)loadData:(NSDictionary *)datos{
    [super loadData:datos];
    [self.sliderCurrent setText:[[datos objectForKey:SLDR_INIT]stringValue]];
    [self.slider setValue:[[datos objectForKey:SLDR_INIT]floatValue]];
}

- (NSDictionary *)getData
{
    [super getData];
    UISlider *sldr = [[[[self subviews]objectAtIndex:0]subviews]objectAtIndex:0];
    int value = (int)sldr.value;
    NSString *valor = [NSString stringWithFormat:@"%d",value];
    NSDictionary *interData = [[NSDictionary alloc]initWithObjectsAndKeys:TYPE_LBLSLDR ,VW_TYPE,valor,DATE_TEXT, nil];
    //NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:key, VW_NAME, interData,DATE_DATE, nil];
    return dicData;
}

@end
