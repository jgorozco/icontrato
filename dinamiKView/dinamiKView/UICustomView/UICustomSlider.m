//
//  UICustomSlider.m
//  dinamiKView
//
//  Created by pcoellov on 8/14/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "UICustomSlider.h"

@implementation UICustomSlider

- (id)initFromDict:(NSDictionary *)dc
{
    self = [super initFromDict:dc];
    if (self)
    {
        if (([dc objectForKey:VW_X] == nil)||([dc objectForKey:VW_Y] == nil)||([dc objectForKey:VW_W] == nil)||([dc objectForKey:VW_H] == nil))
        {
            self = nil;
        }
        else {
            UISlider *cstmsldr = [[UISlider alloc] initWithFrame:[UICustomUtils rectFromDicOriginZero:dc]];
            [cstmsldr setMaximumValue:[[dc objectForKey:SLDR_MAX]floatValue]];
            [cstmsldr setMinimumValue:[[dc objectForKey:SLDR_MIN]floatValue]];
            [cstmsldr setValue:0.0];
            [cstmsldr setBackgroundColor:[UIColor clearColor]];
            [self addSubview:cstmsldr];
            NSLog(@"UICustomSlider creado.");
        }
    }
    return self;
}

- (NSDictionary *)getData
{
    return dicData;
}


@end
