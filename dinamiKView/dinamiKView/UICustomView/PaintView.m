//
//  PaintView.m
//  PaintApp
//
//  Created by Carlos Balduz Bernal on 03/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PaintView.h"

@implementation PaintView

- (id)initFromDict:(NSDictionary *)dc 
{
    self = [super initFromDict:dc];
    if (self) {
        if (([dc objectForKey:VW_X] == nil)||([dc objectForKey:VW_Y] == nil)||([dc objectForKey:VW_W] == nil)||([dc objectForKey:VW_H] == nil))
        {
            self = nil;
        }
        else {
            path = CGPathCreateMutable();
            [self clearDraw:nil];
            [self addSubview:[self botonClear:[UICustomUtils rectFromDic:dc]]];
            self.backgroundColor=[UIColor whiteColor];
            wasSigned=NO;
            NSLog(@"PaintView creado.");
        }
    }
    return  self;
}

-(UIButton *)botonClear: (CGRect)frame
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self 
               action:@selector(clearDraw:)
     forControlEvents:UIControlEventTouchDown];
    [button setTitle:@"X" forState:UIControlStateNormal];
    button.frame = CGRectMake(frame.size.width-30,0,30,30);
    return button;
}

-(BOOL)isSigned
{
    return wasSigned;
}

-(void)beginDraw:(CGPoint)point
{
//    path = CGPathCreateMutable();
    wasSigned=YES;
    if(CGPointEqualToPoint(previousPoint, CGPointZero))
    {
    previousPoint = lastPoint=point;
    
    }else {
        lastPoint=point;
        
    }
    [self moveDraw:point];
   // [self setNeedsDisplay];

}
-(void)moveDraw:(CGPoint)point
{
    previousPoint = lastPoint;
    lastPoint = point;
    
    
    [self setNeedsDisplay];

}
-(void)clearDraw:(id)sender
{
     path = CGPathCreateMutable();
    previousPoint=CGPointZero;
    lastPoint=CGPointZero;
    [self setView:nil];
    NSArray *subvistas = [self subviews];
    if (subvistas.count > 1) {
        [[subvistas lastObject]removeFromSuperview];
    }
    [self setNeedsDisplay];
    wasSigned=NO;
}
-(void)endDraw:(CGPoint)point
{
    
}

-(void) setView: (UIImage *) imagen
{
    CGRect recta = CGRectMake(0,0,self.frame.size.width, self.frame.size.height);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:recta];
    //[imageView setImage:imagen];
    [self addSubview:imageView];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {    
    wasSigned=YES;
    NSLog(@"touchBegan");
    lastPoint = [[touches anyObject] locationInView:self];
    previousPoint = [[touches anyObject] previousLocationInView:self];
    [self setNeedsDisplay];    
}

/*-(void)setImage:(UIImage *)imagen
{
    CGRect recta=CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    UIImageView *imagenview=[[UIImageView alloc] initWithFrame:recta];
    [imagenview setImage:imagen];
    [self addSubview:imagenview];
    


}*/

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {    
    lastPoint = [[touches anyObject] locationInView:self];
    previousPoint = [[touches anyObject] previousLocationInView:self];
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGPathMoveToPoint(path, NULL, previousPoint.x, previousPoint.y);
    CGPathAddLineToPoint(path, NULL, lastPoint.x, lastPoint.y);
    CGContextAddPath(context, path);
    CGContextSetLineWidth(context, 2.0);
	[[UIColor blackColor] setStroke];
    CGContextDrawPath(context, kCGPathFillStroke);
}


@end
