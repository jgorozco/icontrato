//
//  UICustomView.h
//  dinamiKView
//
//  Created by pcoellov on 8/10/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface UICustomView : UIView
{
    NSDictionary *dicConfig;
    NSMutableDictionary *dicData;
    NSObject *mirecibidor;
}

- (id)initFromDict:(NSDictionary *)dc;
- (void)loadData:(NSDictionary *)datos;
- (BOOL)validate;
- (NSDictionary *)getData;
- (NSString *)getName;
- (void)setReciver:(NSObject *)recibidor;

@end
