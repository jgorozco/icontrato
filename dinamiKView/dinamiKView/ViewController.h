//
//  ViewController.h
//  dinamiKView
//
//  Created by jose garcia orozco on 06/08/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaintView.h"
#import "UICustomUtils.h"
#import "UICustomTextField.h"
#import "UICustomLabel.h"
#import "PaintView.h"
#import "UILabelStepper.h"
#import "UICustomSlider.h"
#import "UICustomDatePicker.h"
#import "UILabelSlider.h"
#import "UICustomPickerView.h"
#import "UIUnknownClass.h"
#import "UICustomButton.h"
#import "UICustomButtonDelegate.h"
#import "UICustomScrollTable.h"


@interface ViewController : UIViewController <UICustomButtonDelegate>
{
    CGRect recuadroOrigen;

}
@property (nonatomic,retain) UIView *cstbtn;
@property (nonatomic,retain) NSMutableArray *correspondenciaTagNombre;
@property (nonatomic,retain) NSDictionary *dicDatosElementosVisibles;
@property (nonatomic,retain) NSMutableDictionary *diccionariosElementosVisibles;
@property (nonatomic,retain) NSMutableDictionary *viewsGeneradas;

@end