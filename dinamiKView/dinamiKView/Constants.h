//
//  NSObject_Constants.h
//  dinamiKView
//
//  Created by pcoellov on 8/13/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <Foundation/Foundation.h>

// CONSTANTES GENERALES DE LOS ELEMENTOS
#define VW_NAME @"name" // indica el nombre del elemento
#define VW_X @"x" // indica la posicion 'x' del elemento
#define VW_Y @"y" // indica la posicion 'y' del elemento
#define VW_W @"w" // indica el ancho del elemento
#define VW_H @"h" // indica la altura del elemento
#define TF_STYLE @"style" // variable del estilo del elemento
#define TF_STYLE_ROUND_RECT @"UITextBorderStyleRoundedRect" // estilo "roundrect" del textfield
#define VW_COLOR @"color" // color del elemento
#define VW_TXT_COLOR @"textcolor" // Color del texto del elemento
#define VW_VALIDATION @"validate" // Tipo de validación que tiene el elemento
#define TF_TXT @"text" // texto del textField
#define TF_PHOLDER @"placeholder" // texto que aparece a modo de información en el textfield
#define TF_VALIDATION_MAIL @"email" // tipo de validacion de email del textField
#define TF_VALIDATION_NUM @"num" // tipo de validacion numerica del textField
#define TF_VALIDATION_TXT @"text" // tipo de validacion de texto del textField
#define LB_FONTSIZE @"fontSize" // indica el tamanyo de la fuente del label
#define DB_FIELD_NAME @"dbname"//nombre del campo en base de datos
#define DB_MANDATORY @"mandatory"//indica que el campo es obligatorio, se validara
#define VW_TYPE @"type" // tipo del elemento visual
#define PV_VALIDATION_SIGN @"sign" // validacion de firma en paintView
#define LS_TITLE @"title" // titulo principal del LabelSteeper
#define LS_SUBTITLE @"subtitle" // subtitulo principal del LabelSteeper
#define LS_BT @"biggerthan" // valor de comprobacion (tiene que ser mayor que este numero)
#define SLDR_MAX @"max" // valor máximo del slider
#define SLDR_MIN @"min" // valor mínimo del slider
#define SLDR_INIT @"init" // valor inicial del slider
/* El formato que llegue tendrá 4 tipos: contador, fecha, cita y hora */
#define DP_FORMAT @"format" // formato del Date picker
#define DP_FORMAT_TIMER @"contador" //formato cuentra atrás
#define DP_FORMAT_DATE @"fecha" // formato fecha DD/MM/YYYY
#define DP_FORMAT_DATE_TIME @"cita" // formato FECHA/HORAS/MINUTOS
#define DP_FORMAT_TIME @"hora" // formato HH/MM/SS
#define PCKR_DEF @"default" // valor por defecto para comprobar que se ha elegido un elemento
#define BTN_ACT @"action" // accion que tendrá el boton
#define BTN_NAME @"name" // nombre del botón
#define BTN_TEXT @"text" // texto del botón

//TIPOS DE ACCIONES DE LOS BOTONES
#define VAL_VALIDATE @"validate" // Botón que valida el formulario
#define VAL_GENERATE @"generate" // Boton que valida formulario y genera JSON + PDF de salida

//CONSTANTES DEL JSON DE DATOS
#define DATE_NAME @"datos" // todos los datos de los elementos
#define DATE_DATE @"data" //  datos internos concretos de cada elemento
#define DATE_TYPE @"type"  // tipo de elemento dentro del JSON
#define DATE_TEXT @"text"  // texto inicial de los textfield
#define DATE_NUM_INIT @"init" // numero inicial de los labelsteeper o labelslider
#define DATE_NUM_MIN @"min"  // numero minimo inicial de los labelslider
#define DATE_NUM_MAX @"max" // numero maximo inicial de los labelslider
#define DATE_SELECTED @"selected" // valor seleccionado inicialmente por los pickerview
#define DATE_DEFAULT @"default" // valor por defecto para los pickerview
#define DATE_LIST @"list" // lista de elementos para el pickerview
#define DATE_VALUE @"value" // numero introducido por el usuario para el getData

//CONSTANTES PARA LA TABLA
#define TABLE_COL_NUMBER @"columnNumber" // Numero de columnas de la tabla de datos
#define TABLE_COLUMNS @"columns" // Parametro para coger del JSON los datos de las columnas
#define TABLE_HIDDEN @"hidden" // Parametro booleando para ocultar o visualizar elementos
#define TABLE_H_ROW @"heightRow" // altura de las filas de la tabla de datos
#define TABLE_F_ROW @"formRow" // Parametro que se usa, en la creación de la tabla, para ir determinando la posicion y de cada fila: Inicializado a 0, se le va dando un valor el cual va adquiriendo la fila y se coloca según dicho valor


//TIPOS DE CUSTOMVIEWS
#define TYPE_TF @"UICustomTextField" // UIView + UITextField
#define TYPE_LBL @"UICustomLabel" // UIView + UICustomLabel
#define TYPE_LBLSLDR @"UILabelSlider" // UIView + UISlider
#define TYPE_LBLSTPR @"UILabelStepper" // UIView + UILabelStepper
#define TYPE_PNTVW @"PaintView" // UIView + UIPaintView
#define TYPE_SLDR @"UICustomSlider" // UIView + UISlider + 3UILabel
#define TYPE_DTPCKR @"UICustomDatePicker" // UIView + UIDatePicker
#define TYPE_PCKRVW @"UICustomPickerView" // UIView + UIPickerView
#define TYPE_BTN @"UICustomButton" // UIView + UICustomButton