//
//  ViewController.m
//  dinamiKView
//
//  Created by jose garcia orozco on 06/08/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "ViewController.h"
#import "UICustomButton.h"
@interface ViewController () 

@end

@implementation ViewController
@synthesize correspondenciaTagNombre,diccionariosElementosVisibles,viewsGeneradas,dicDatosElementosVisibles,cstbtn;


-(void)loadJSONViews
{
    NSString *jsonPath=[[NSBundle mainBundle] pathForResource:@"formFile" ofType:@"json"];
    NSDictionary *metaElements =[NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:jsonPath] options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"Diccionario:%@",metaElements);    
    NSArray *arrayElementos=[metaElements objectForKey:@"page"];
    diccionariosElementosVisibles=[[NSMutableDictionary alloc] init];
    viewsGeneradas=[[NSMutableDictionary alloc] init];
    for (NSDictionary *elementoVisual in arrayElementos)
    {
        [diccionariosElementosVisibles setObject:elementoVisual forKey:[elementoVisual objectForKey:VW_NAME]];
        Class clssgnrtd = NSClassFromString([elementoVisual objectForKey:VW_TYPE]);
        id object = [[clssgnrtd alloc]init];
        if ([object isKindOfClass:[UICustomButton class]])
        {
            UICustomButton *btn=(UICustomButton *)object;
            [btn setParentHandler:self];
            object=btn;
        }
        object = [object initFromDict:elementoVisual];
        [object setReciver:self];
        if (object == nil)
        {
            UIUnknownClass *nknw = [[UIUnknownClass alloc] initFromDict:elementoVisual];
            [self.view addSubview:nknw];
        }
        else {
         //   [self addDictToView:elementoVisual view:object];
            [self.view addSubview:object];
            
            [viewsGeneradas setObject:object forKey:[elementoVisual objectForKey:VW_NAME]];
        }
    }
 //   NSLog(@"ADDING:%@",viewsGeneradas);
    NSLog(@"Cargadas vistas de elementos correctamente.");
}

//for cada uno de los elementos en el load
    //cogemos el diccionario de un determinado nombre
    // buscamos el objeto visual de ese nombre cargado
    //llamamso a su load.
-(void)loadJSONdata{
 //    NSLog(@"2222ADDING:%@",viewsGeneradas);
    NSString *jsonPath=[[NSBundle mainBundle] pathForResource:@"dataFile" ofType:@"json"];
    NSDictionary *elementData =[NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:jsonPath] options:NSJSONReadingMutableContainers error:nil];
    elementData=[elementData objectForKey:@"datos"];
    for (NSString *cvname in elementData)
    {
        UICustomView *cv=[viewsGeneradas objectForKey:cvname];
        if (cv){
            [cv loadData:[elementData objectForKey:cvname]];
        }
    }

}


-(void)loadJSONdata2
{
    NSString *jsonPath=[[NSBundle mainBundle] pathForResource:@"dataFile" ofType:@"json"];
    NSDictionary *metaElements =[NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:jsonPath] options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"DicDatos:%@",metaElements);
    dicDatosElementosVisibles = [UICustomUtils JSONtoDict:metaElements];
    for (NSDictionary *elementoVisual in diccionariosElementosVisibles)
    {
        NSString *objectName=[elementoVisual valueForKey:VW_NAME];
        NSDictionary *datos = [dicDatosElementosVisibles objectForKey:objectName];
        UICustomView *cv=[viewsGeneradas objectForKey:objectName];
        //NSString *cstmClss = [elementoVisual objectForKey:VW_NAME];
        //UICustomView *custom = [viewsGeneradas objectForKey:cstmClss];
        if ((datos != nil)&&(cv !=nil)){
            Class clssgnrtd = NSClassFromString([elementoVisual objectForKey:VW_TYPE]);
            id object = [[clssgnrtd alloc]init];
            if ([object isKindOfClass:[UICustomScrollTable class]])
            {
                NSString *jsonPath=[[NSBundle mainBundle] pathForResource:[elementoVisual objectForKey:DATE_DATE] ofType:@"json"];
                datos =[NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:jsonPath] options:NSJSONReadingMutableContainers error:nil];
            }
            [cv loadData:datos];
        }
    }
   // NSLog(@"Cargados datos de elementos incorrectamente. (-.-') ");
}

-(NSString *)getDataToJSON
{
    NSMutableArray *datos = [[NSMutableArray alloc]init];
    for (NSDictionary *elementoVisual in diccionariosElementosVisibles)
    {
        Class clssgnrtd = NSClassFromString([elementoVisual objectForKey:VW_TYPE]);
        id object = [[clssgnrtd alloc]init];
        NSDictionary *vuelta = [object getData];
        if ([vuelta objectForKey:DATE_DATE] != nil){
            [datos addObject:vuelta];
        }
    }
    NSMutableDictionary *final = [[NSMutableDictionary alloc]initWithObjectsAndKeys:datos,DATE_NAME, nil];
    dicDatosElementosVisibles = final;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:final options:0 error:nil];
    NSString *jsonString = [[NSString alloc] init];
    if (!jsonData) {
        NSLog(@"Error creando el JSON");
        jsonString = [[NSString alloc]initWithString:@"(null json)"];
    }
    else {
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}


-(void)addDictToView:(NSDictionary *)dic view:(UIView *)vista
{
    [correspondenciaTagNombre addObject:[dic objectForKey:VW_NAME]];
    [vista setTag:correspondenciaTagNombre.count-1];

}


-(void)viewWillAppear:(BOOL)animated
{
    correspondenciaTagNombre=[[NSMutableArray alloc]init];
    viewsGeneradas=[[NSMutableDictionary alloc]init];
    NSLog(@"viewDidLoad");
    [self loadJSONViews];
    [self loadJSONdata];
    

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"viewDidLoad");  
}



-(void)viewDidAppear:(BOOL)animated
{
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)actionSelected:(NSString *)action
{
    NSLog(@"Action selected: '%@'",action);
    if ([action isEqualToString:VAL_VALIDATE])
    {
        [self validateFields];
    }
    if ([action isEqualToString:VAL_GENERATE])
    {
        // Validar contrato
        if ([self getFieldsAndValidate])
        {
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Formulario validado"
                                                              message:@"El formulario esta relleno correctamente. Generando JSON y PDF."
                                                             delegate:nil
                                                    cancelButtonTitle:@"Aceptar"
                                                    otherButtonTitles:nil];
            
            [message show];
            NSString *JSONsalida = [self getDataToJSON];
            NSLog(@"JSONSalida::.%@",JSONsalida);
            // generar PDF del contrato
        }
        else {
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Necesita Validar"
                                                              message:@"Para poder generar el PDF necesita disponer de una previa validación del contrato."
                                                             delegate:nil
                                                    cancelButtonTitle:@"Aceptar"
                                                    otherButtonTitles:nil];
            
            [message show];
        }
    }
}

- (void) validateText: (id)sender
{
    int element=[sender tag];
    NSString *lmnt = [NSString stringWithFormat:@"%d",element];
    NSDictionary *elementoAValidar=[diccionariosElementosVisibles objectForKey:lmnt];
    NSLog(@"Validando:%@",elementoAValidar);
    NSString *validacion=[elementoAValidar objectForKey:VW_VALIDATION];
    if (validacion)
    {
        if ([validacion isEqualToString:TF_VALIDATION_MAIL])
        {
            if (![UICustomUtils validateString:[sender text] type:TF_VALIDATION_MAIL])
            {
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Valor Erroneo"
                                                                  message:@"Se esperaba un mail, el dato no tiene ese formato"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                
                [message show];
                [sender setText:[elementoAValidar objectForKey:TF_TXT]];
            }
        }
        if ([validacion isEqualToString:TF_VALIDATION_NUM])
        {
            if (![UICustomUtils validateString:[sender text] type:TF_VALIDATION_NUM])
            {
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Valor Erroneo"
                                                                  message:@"Se esperaba un numero el dato no tiene ese formato"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                
                [message show];
                [sender setText:[elementoAValidar objectForKey:TF_TXT]];
            }
        }
    
    }
   }



- (void)validateFields {
    if ([self getFieldsAndValidate])
    {
        //[self getFieldsAndStore];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Formulario validado"
                                                          message:@"Se ha rellenado y firmado correctamente el formulario."
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
    }else{
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"formulario no validado"
                                                          message:@"el formulario no está validado, reviselo"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
    
    }
    
}



-(BOOL)getFieldsAndValidate
{
    BOOL valid=YES;
    for (NSDictionary *elemento in diccionariosElementosVisibles)
    {
        // Acceso al elemento del diccionario
        /*NSLog(@"dicc::. %@",diccionariosElementosVisibles);
        NSArray *keys = [diccionariosElementosVisibles allKeys];
        NSString *key = [keys objectAtIndex:i];
        NSDictionary *objetoSelecionado=[diccionariosElementosVisibles objectForKey:key];*/
        if ([elemento objectForKey:DB_MANDATORY])
        {
            NSString *tipo = [elemento objectForKey:VW_TYPE];
            if ([tipo isEqualToString:@"PaintView"])
            {
                PaintView *firm = [viewsGeneradas objectForKey:[elemento objectForKey:VW_NAME]];
                if (!([UICustomUtils validateSign:firm])){
                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Se requiere una firma."
                                                                      message:@"El formulario no ha podido ser validado debido a que requiere una firma."
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles:nil];
                    [message show];
                    valid= NO;
                }
            }
            if ([tipo isEqualToString:@"UILabelStepper"])
            {
                int min = [[elemento objectForKey:LS_BT]intValue];
                UILabel *lbl = [[[[[viewsGeneradas objectForKey:@"labelStepper1"]subviews]objectAtIndex:0]subviews]objectAtIndex:1];
                int valor = lbl.text.intValue;
                if (!([UICustomUtils validateMinimum:valor :min])){
                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Se requiere un mínimo."
                                                                      message:[NSString                             
                                                                               stringWithFormat:@"El formulario no ha podido ser validado debido a que requiere un valor mínimo en el campo '%@'.",[elemento objectForKey:LS_TITLE]]
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles:nil];
                    [message show];
                    valid= NO;
                }
            }
            if ([tipo isEqualToString:@"UICustomLabel"] || [tipo isEqualToString:@"UICustomTextField"]){
                // Acceso al elemento del diccionario
                NSString *key = [elemento objectForKey:VW_NAME];
                UITextField *txtf = [[[viewsGeneradas valueForKey:key]subviews]objectAtIndex:0];
                NSString *datos = [txtf text];
                if ((!datos)||(datos.length==0)||([datos isEqualToString:@""]))
                {
                    valid=NO;
                }
            } 
        }
    }
    return valid;
}

-(void)getFieldsAndStore
{
    for (int i=0;i<correspondenciaTagNombre.count;i++)
    {
        // Acceso al elemento del diccionario
        NSArray *keys = [diccionariosElementosVisibles allKeys];
        NSString *key = [keys objectAtIndex:i];
        NSDictionary *objetoSelecionado=[diccionariosElementosVisibles objectForKey:key];
        if ([objetoSelecionado objectForKey:DB_FIELD_NAME])
        {
            NSString *fieldname=[objetoSelecionado objectForKey:DB_FIELD_NAME];
            // Acceso al elemento del diccionario
            NSArray *keys = [viewsGeneradas allKeys];
            NSString *key = [keys objectAtIndex:i];
            NSString *datos=[[viewsGeneradas valueForKey:key] text];
            if ((!datos)||(datos.length==0)||([datos isEqualToString:@""]))
            {
                NSLog(@"el campo %@ no contiene datos",fieldname);

            }else{
                NSLog(@"Tenemos para el campo %@ ==%@",fieldname,datos);
            }
        }
        
        
    }

}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSLog(@"---textFieldShouldBeginEditing");
    return YES;
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"---->textFieldDidEndEditing");
    if (!CGRectEqualToRect(recuadroOrigen, CGRectNull))
    {
        const float movementDuration = 0.3f;
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
//        recuadroOrigen=self.view.frame;
        self.view.frame = recuadroOrigen;
        [UIView commitAnimations];
        recuadroOrigen=CGRectNull;

    }
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    UIInterfaceOrientation interfaceOrientation = self.interfaceOrientation;

    CGSize keyboardsize=CGSizeMake(self.view.center.x*2, 400.0);
    CGPoint p=[textField.superview convertPoint:textField.center toView:self.view];
    
    CGRect rect;
     if ((interfaceOrientation==UIInterfaceOrientationLandscapeLeft)||
         (interfaceOrientation==UIInterfaceOrientationLandscapeRight))
     {
        rect=CGRectMake(0, self.view.frame.size.width-keyboardsize.height, self.view.frame.size.height, keyboardsize.width);
     }else{
         rect=CGRectMake(0, self.view.frame.size.height-keyboardsize.height, self.view.frame.size.width, keyboardsize.height);
     }

    NSLog(@"---->%@",NSStringFromCGRect(self.view.frame));
    NSLog(@"key:%@ in point%@",NSStringFromCGRect(rect),NSStringFromCGPoint(p));
    if ((CGRectContainsPoint(rect, p))&&(up)){
    
    NSLog(@"--->textFieldDidBeginEditing:%d",up);
    int movementDistancex = 0;
    int movementDistancey = 0;
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft)
    {
        movementDistancex=180;
    }
    if (interfaceOrientation==UIInterfaceOrientationLandscapeRight)
    {
        movementDistancex=-180;

    }
    if (interfaceOrientation==UIInterfaceOrientationPortrait)
    {
        movementDistancey=180;
    }
    if (interfaceOrientation==UIInterfaceOrientationPortraitUpsideDown)
    {
        movementDistancey=-180;
    }
    //   const int movementDistancey = 180; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movementx = (up ? -movementDistancex : movementDistancex);
    int movementy = (up ? -movementDistancey : movementDistancey);
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    recuadroOrigen=self.view.frame;
    self.view.frame = CGRectOffset(self.view.frame, movementx, movementy);
    [UIView commitAnimations];
    }
}



@end
