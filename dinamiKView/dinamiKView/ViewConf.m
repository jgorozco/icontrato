//
//  ViewConf.m
//  dinamiKView
//
//  Created by jose garcia orozco on 06/08/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "ViewConf.h"

@implementation ViewConf

+(UICustomTextField *)createCustomTextField:(NSDictionary *)dc
{
    UICustomTextField *cstmtxtfld = [[UICustomTextField alloc] initFromDict:dc];
    return cstmtxtfld;
}

/*+(UITextField *)createTextField:(NSDictionary *)dc{

    UITextField *tfl=[[UITextField alloc] initWithFrame:[ViewConf rectFromDic:dc]];
    NSString *value=[dc objectForKey:TF_STYLE];
    if ((value)&&([value isEqualToString:TF_STYLE_ROUND_RECT]))
        [tfl setBorderStyle:UITextBorderStyleRoundedRect];
    value=[dc objectForKey:TF_TXT];
    if (value)
        [tfl setText:value];
    value=[dc objectForKey:TF_PHOLDER];
    if (value)
        [tfl setPlaceholder:value];
    value=[dc objectForKey:VW_COLOR];
    if (value)
        [tfl setBackgroundColor:[self colorFromHexString:value]];
    value=[dc objectForKey:VW_TXT_COLOR];
    if (value)
        [tfl setTextColor:[self colorFromHexString:value]];
    
    value=[dc objectForKey:VW_VALIDATION];
    if ((value)&&([value isEqualToString:TF_VALIDATION_MAIL]))
    {
        [tfl setKeyboardAppearance:UIKeyboardAppearanceDefault];
        [tfl setKeyboardType:UIKeyboardTypeEmailAddress];
    }
    if ((value)&&([value isEqualToString:TF_VALIDATION_NUM]))
    {
        [tfl setKeyboardAppearance:UIKeyboardAppearanceDefault];
        [tfl setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    }
    return tfl;
}*/

+(UICustomLabel *)createCustomLabel:(NSDictionary *)dc
{
    UICustomLabel *cstmlbl = [[UICustomLabel alloc] initFromDict:dc];
    return cstmlbl;
}

/*+(UILabel *)createLabel:(NSDictionary *)dc
{
    UILabel *lbl=[[UILabel alloc]initWithFrame:[ViewConf rectFromDic:dc]];
    NSString *value=[dc objectForKey:TF_TXT];
    if (value)
        [lbl setText:value];
    value=[dc objectForKey:VW_COLOR];
    if (value)
        [lbl setBackgroundColor:[self colorFromHexString:value]];
    value=[dc objectForKey:VW_TXT_COLOR];
    if (value)
        [lbl setTextColor:[self colorFromHexString:value]];
    value=[dc objectForKey:LB_FONTSIZE];
    if (value)
        lbl.font = [UIFont fontWithName:@"Helvetica" size:[value intValue]];
    return lbl;
}*/


+(PaintView *)createPaintView:(NSDictionary *)dc 
{
    PaintView *firma = [[PaintView alloc] initFromDict:dc];
    return firma;
}

+(UILabelStepper *)createLabelSteeper:(NSDictionary *) dc
{
    UILabelStepper *lSteeper = [[UILabelStepper alloc] initFromDict:dc];
    return lSteeper;
}

+(UICustomSlider *)createCustomSlider:(NSDictionary *)dc
{
    UICustomSlider *cstmSldr = [[UICustomSlider alloc] initFromDict:dc];
    return cstmSldr;
}

/*+(UISlider *) createSlider:(NSDictionary *)dc
{
    UISlider *slider = [[UISlider alloc] initWithFrame:[UICustomUtils rectFromDic:dc]];
    [slider setMaximumValue:[[dc objectForKey:SLDR_MAX]floatValue]];
    [slider setMinimumValue:[[dc objectForKey:SLDR_MIN]floatValue]];
    [slider setBackgroundColor:[UIColor clearColor]];
    return slider;
}*/

+(UILabelSlider *) createLabelSlider:(NSDictionary *)dc
{
    UILabelSlider *labelSlider = [[UILabelSlider alloc] initFromDict:dc];
    return labelSlider;
}

+(UICustomDatePicker *)createCustomDatePicker:(NSDictionary *)dc
{
    UICustomDatePicker *cstmdtpckr = [[UICustomDatePicker alloc] initFromDict:dc];
    return cstmdtpckr;
}

/*+(UIDatePicker *) createDatePicker:(NSDictionary *)dc
{
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:[UICustomUtils rectFromDic:dc]];
    if ([[dc objectForKey:DP_FORMAT]isEqualToString:DP_FORMAT_DATE]) 
        datePicker.datePickerMode = UIDatePickerModeDate;
    if ([[dc objectForKey:DP_FORMAT]isEqualToString:DP_FORMAT_DATE_TIME]) 
        datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    if ([[dc objectForKey:DP_FORMAT]isEqualToString:DP_FORMAT_TIME]) 
        datePicker.datePickerMode = UIDatePickerModeTime;
    if ([[dc objectForKey:DP_FORMAT]isEqualToString:DP_FORMAT_TIMER]) 
        datePicker.datePickerMode = UIDatePickerModeCountDownTimer;
    return datePicker;
}*/

+(UICustomView *)createCustomView:(NSDictionary *)dc
{
    UICustomView *cstmvw = [[UICustomView alloc] initFromDict:dc];
    return cstmvw;
}

+(UICustomPickerView *)createCustomPickerView:(NSDictionary *)dc
{
    UICustomPickerView *cstmpckrvw = [[UICustomPickerView alloc] initFromDict:dc];
    return cstmpckrvw;
}

+(BOOL)validateString:(NSString *)data type:(NSString *)type
{
    
    if (!data)
        return NO;
    if ([type isEqualToString:TF_VALIDATION_MAIL])
    {
        BOOL stricterFilter = YES;
        NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
        NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        return [emailTest evaluateWithObject:data];
    }
    
    if ([type isEqualToString:TF_VALIDATION_NUM])
    {
        bool result = false;
        NSScanner *scan = [NSScanner scannerWithString:data];
        if (![scan scanFloat:NULL] || ![scan isAtEnd])
        {
            result=NO;
        }
        else
        {
           result=YES;
        }
        return result;
    }
    
    return YES;
    
}

+(BOOL)validateSign:(PaintView *)firma 
{
    return [firma isSigned];
}

+(BOOL)validateMinimum:(UILabelStepper *)lSteeper:(int)min
{
    return lSteeper.labelContenido.text.intValue >= min;
}

@end
