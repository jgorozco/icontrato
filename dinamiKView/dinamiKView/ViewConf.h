//
//  ViewConf.h
//  dinamiKView
//
//  Created by jose garcia orozco on 06/08/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaintView.h"
#import "UICustomUtils.h"
#import "UILabelStepper.h"
#import "UICustomView.h"
#import "UILabelSlider.h"
#import "Constants.h"
#import "UICustomLabel.h"
#import "UICustomTextField.h"
#import "UICustomSlider.h"
#import "UICustomDatePicker.h"
#import "UICustomPickerView.h"


@interface ViewConf : NSObject

//+(UITextField *)createTextField:(NSDictionary *) dc;
//+(UILabel *)createLabel:(NSDictionary *) dc;
+(PaintView *)createPaintView:(NSDictionary *) dc;
+(UILabelStepper *)createLabelSteeper:(NSDictionary *) dc;
//+(UISlider *)createSlider:(NSDictionary *)dc;
//+(UIDatePicker *)createDatePicker:(NSDictionary *)dc;
+(UILabelSlider *)createLabelSlider:(NSDictionary *)dc;
+(UICustomLabel *)createCustomLabel:(NSDictionary *)dc;
+(UICustomTextField *)createCustomTextField:(NSDictionary *)dc;
+(UICustomSlider *)createCustomSlider:(NSDictionary *)dc;
+(UICustomDatePicker *)createCustomDatePicker:(NSDictionary *)dc;
+(UICustomPickerView *)createCustomPickerView:(NSDictionary *)dc;

+(BOOL)validateString:(NSString *)data type:(NSString *)type;
+(BOOL)validateSign:(PaintView *)firma;
+(BOOL)validateMinimum:(UILabelStepper *)lSteeper:(int) min;
@end
